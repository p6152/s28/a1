
// Answer #3
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json())
.then( data => {console.log(data)})



// Answer #4 
fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json())
.then( data => {

    data.map(dataTitle => {console.log(`Title: ${dataTitle.title}`)})
})


//Answer #5
fetch(`https://jsonplaceholder.typicode.com/todos/1`)
.then(data => data.json())
.then(data => {console.log(data)})

//Answer #6

fetch(`https://jsonplaceholder.typicode.com/todos`)
.then( data => data.json())
.then( data => {

    data.map(dataTitle => {console.log(`Title: ${dataTitle.title} Completed: ${dataTitle.completed}`)})
})

//Answer #7
fetch(`https://jsonplaceholder.typicode.com/todos/`, {
    method: "POST",
    headers: {
        "Content-Type": "application/json"
    },
    body: JSON.stringify({
        title: "New Title",
        body: "New Body",
        userId: 1
    })
})
.then( response => response.json() )
.then( response => {
    console.log(response)
})

//Answer #8
fetch(`https://jsonplaceholder.typicode.com/todos/1`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            title: "Updated Post",
            body: "New updated post!",
            userId: 1
        })
    })
    .then(res => res.json())
    .then(res => {
        console.log(res)
    })

// Answer #9

    